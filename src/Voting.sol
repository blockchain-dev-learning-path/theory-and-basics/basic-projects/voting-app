// SPDX-License-Identifier: MIT
pragma solidity ^0.8.18;

/**
 * This is a simple "Voting contract" that should allow all registered +18y.o. voters to vote for a registered candidate.
 * Candidates can only be added by the owner of this contract.
*/


contract Voting {

    address private immutable i_owner;
    uint256 public votingStarts;
    uint256 public votingEnds;
    uint256 public totalVotesCast;
    bool private resultsPrepared;


    constructor(uint256 _start, uint256 _end) {
        i_owner = msg.sender;
        votingStarts = _start;
        votingEnds = _end;
        totalVotesCast = 0;
        resultsPrepared = false;
    }


    struct Citizen {
        string name;
        string lastName;
        uint8 age;
        uint256 id;
    }

    struct Candidate {
        Citizen citizen;
        uint256 voteCount;
        uint256 percentageVoted;
    }

    struct ElectionResults {
        string winnersName;
        uint256 winnersVotePercentage;
        uint256 votesCastInTotal;
        uint256 percentageVoted;
    }

    ElectionResults public electionResults;


    Citizen[] public voters;
    Candidate[] public candidates;

    mapping(address => bool) public isRegisteredVoterMap;
    mapping(address => bool) public hasVotedMap;
    mapping(address => Citizen) public addressXcitizenMap;


    // MODIFIERS:

    modifier onlyOwner() {
        require(msg.sender == i_owner, "Only the owner can perform this action.");
        _;
    }

    modifier onlyOfLegalAge() {
        Citizen memory citizen = addressXcitizenMap[msg.sender];
        require(citizen.age >= 18, "All voters must be over 18 years old in order to be allowed to vote.");
        _;
    }

    modifier duringVotingPeriod() {
        require(block.timestamp >= votingStarts && block.timestamp <= votingEnds, "Voting is not active.");
        _;
    }

    modifier isRegisteredVoter() {
        require(isRegisteredVoterMap[msg.sender] == true, "All voters must be registered in order to vote.");
        _;
    }


    // GETTERS:

    function getRegisteredVoters() public view returns(Citizen[] memory) {
        return voters;
    }

    function getCandidates() public view returns(Candidate[] memory) {
        return candidates;
    }

    function getVotedPercentage() public view returns(uint256) {
        uint256 totalRegisteredVoters = voters.length;
        if (totalRegisteredVoters == 0) {
            return 0;
        }

        uint256 percentage = (totalVotesCast * 10000) / totalRegisteredVoters;

        return percentage / 100;
    }

    function getCandidateVotesPercentage(uint256 candidateIndex) public view returns(uint256) {
        if (totalVotesCast == 0) {
            return 0;
        }

        uint256 candidateVoteCount;

        candidateVoteCount = candidates[candidateIndex].voteCount;
            

        uint256 percentage = (candidateVoteCount * 10000) / totalVotesCast;
        uint256 result = percentage / 100;

        return result;

    }

    //

    function addCitizen(string memory _name, string memory _lastName, uint8 _age, uint256 _id) public {
        Citizen memory newCitizen = Citizen({
            name: _name,
            lastName: _lastName,
            age: _age,
            id: _id
        });

        addressXcitizenMap[msg.sender] = newCitizen;
    }

    
    function registerVoter() public onlyOfLegalAge {
        // Check voter is not registered
        require(isRegisteredVoterMap[msg.sender] == false, "Voter already registered.");
        // Register voter
        isRegisteredVoterMap[msg.sender] = true;
        // Add citizen to voters array
        Citizen memory citizen = addressXcitizenMap[msg.sender];
        voters.push(citizen);
    }

    // A Candidate must be a registered Citizen first (he can also vote and he also should be of legal age)
    function registerCandidate(Citizen memory _citizen) public onlyOwner {
        candidates.push(
            Candidate({
                citizen: _citizen, 
                voteCount: 0,
                percentageVoted: 0
            })
        );
    }

    function vote(uint8 _candidateIndex) public duringVotingPeriod isRegisteredVoter {
        require(!hasVotedMap[msg.sender], "Only one vote per citizen is allowed. Fraud is a crime.");
        require(_candidateIndex < candidates.length, "Invalid candidate index");
        candidates[_candidateIndex].voteCount++;
        hasVotedMap[msg.sender] = true;
        totalVotesCast++;
    }

    
    function determineElectionWinner() private view returns(string memory) {
        uint256 winner = 0;
        uint256 highestVoteCount = 0;
        bool draw = false;
        
        for(uint256 candidateIndex = 0; candidateIndex < candidates.length; candidateIndex++) {
            if (candidates[candidateIndex].voteCount > highestVoteCount) {
                winner = candidateIndex;
                highestVoteCount = candidates[candidateIndex].voteCount;
                draw = false;
            } else if (candidates[candidateIndex].voteCount == highestVoteCount) {
                draw = true;
            }
        }

        if (draw) {
            return "draw";
        } else {
            string memory winnersName = candidates[winner].citizen.name;
            string memory winnersLastName = candidates[winner].citizen.lastName;

            return string(abi.encodePacked(winnersName, " ", winnersLastName));
        }
    }

    function calculateAllCandidatesVotesPercentage() private {

        for(uint256 candidateIndex = 0; candidateIndex < candidates.length; candidateIndex++) {
            uint256 candidateVotesPercentage = getCandidateVotesPercentage(candidateIndex);
            
            candidates[candidateIndex].percentageVoted = candidateVotesPercentage;
        }

    }

    function prepareElectionResults() public onlyOwner  {
        require(block.timestamp >= votingEnds, "Voting has not ended yet");
        require(!resultsPrepared, "Results already prepared");
        // Get the winner of the election
        string memory winnersName = determineElectionWinner();
        
        uint256 winnersVotePercentage;

        uint256 percentageVoted = getVotedPercentage();

        // Calculate all candidates voting percentage:
        calculateAllCandidatesVotesPercentage();

        
        electionResults = ElectionResults({
            winnersName: winnersName,
            winnersVotePercentage:winnersVotePercentage,
            votesCastInTotal:totalVotesCast,
            percentageVoted:percentageVoted
        });


        resultsPrepared = true;

    }

    function showResults() public view returns(ElectionResults memory, Candidate[] memory) {
        require(block.timestamp >= votingEnds, "Voting has not ended yet");
        return (electionResults, candidates);
    }


}
